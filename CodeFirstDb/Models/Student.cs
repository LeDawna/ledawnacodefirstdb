﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstDb.Models
{
  public class Student
  {
    public int StudentId { get; set; }

    public DateTime? DateOfBirth { get; set; }

    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }

    public virtual ICollection<StudentAddress> StudentAddresses { get; set; }
  }
}
