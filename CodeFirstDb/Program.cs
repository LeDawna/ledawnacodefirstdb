﻿using CodeFirstDb.Data;
using CodeFirstDb.Models;
using System.Data.Entity;
using System.Linq;

namespace CodeFirstDb
{
  class Program
  {
    static void Main(string[] args)
    {
      using(var db = new CodeFirstDbContext())
      {

        //this is eagerly loaded and contains the query results
        var student = db.Students
          .Include(x => x.StudentAddresses)
          .Include(x => x.StudentAddresses.Select(y => y.Address))
          .Single(x => x.StudentId == 1);


        //this is lazy loaded, and is only the query
        var studentQ = db.Students
          .Include(x => x.StudentAddresses)
          .Include(x => x.StudentAddresses.Select(y => y.Address))
          .Where(x => x.StudentId == 1);

        var studentFromMethod = GetStudentQuery(db).Single(x => x.StudentId == 1);

        var activeStudentAddresses = student.StudentAddresses.Where(x => x.InactiveDate == null).ToList();

        var activeAddresses = activeStudentAddresses.Select(x => x.Address).ToList();
      }
    }

    private static IQueryable<Student> GetStudentQuery(CodeFirstDbContext db)
    {
      var sQuery = db.Students
          .Include(x => x.StudentAddresses)
          //.Include(x=> x.Grades)
          .Include(x => x.StudentAddresses.Select(y => y.Address));

      return sQuery;
    }
  }
}
